"use strict"
const getSum = (str1, str2) => {
    if (isNaN(str1) || isNaN(str2) || typeof str1 !== 'string' || typeof str2 !== 'string')
        return false;
    return (Number(str1) + Number(str2)).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    var quanPost = 0;
    var quanComment = 0;
    for (var post of listOfPosts) {
        if (post.author === authorName)
            quanPost++;
        if (post.hasOwnProperty('comments'))
            for (var inPost of post.comments)
                if (inPost.author === authorName)
                    quanComment++;
    }
    return `Post:${quanPost},comments:${quanComment}`;
}

const tickets = (people) => {
    var balance = 0;
    for (var person of people) {
        person = Number(person);
        if (person === 25)
            balance += person;
        else {
            balance = balance - (person - 25);
            if (balance < 0) return 'NO'
            balance += 25;
        }
    }
    return 'YES'
}


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
